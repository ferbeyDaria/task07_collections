package com.epam.controller;

import com.epam.model.BinaryTreeMap;

public class Controller {
    BinaryTreeMap<String, String> treeMap = new BinaryTreeMap<>();
    public String print() {
        String message="";
        return message;
    }

    public String put(String key, String value) {
        return treeMap.put(key, value);
    }

    public String get(String key) {
        return treeMap.get(key);
    }


    public String remove(String key) {
        return treeMap.remove(key);
    }
}
