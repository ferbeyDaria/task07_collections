package com.epam.view;

import com.epam.Main;
import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    public static Logger logger = LogManager.getLogger(Main.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print");
        menu.put("2", "  2 - get");
        menu.put("3", "  3 - put");
        menu.put("4", "  4 - remove");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        logger.info("Put string key and value to print: \n");
    }

    private void pressButton2() {
        logger.info("Put string key to find: \n");
    }

    private void pressButton3() {
        logger.info("Put string key and value to add: \n");
    }

    private void pressButton4() {
        logger.info("Put string key to remove: \n");
    }


    //-----------------------------------------------------


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * Print menu.
     */
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
